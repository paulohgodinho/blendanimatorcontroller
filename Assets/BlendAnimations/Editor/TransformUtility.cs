using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Transform))]
public class TransformUtility : Editor
{	
	string TRANSFORM_COPY_PREFIX = "TRANSFORM_COPY_PREFIX";
	public override void OnInspectorGUI ()
	{
		Transform t = (Transform)target;
		
		// Replicate the standard transform inspector gui
		//EditorGUIUtility.LookLikeControls();
		EditorGUI.indentLevel = 0;
		Vector3 position = EditorGUILayout.Vector3Field("Position", t.localPosition);
		Vector3 rotation = EditorGUILayout.Vector3Field("Rotation", t.localEulerAngles);
		Vector3 scale = EditorGUILayout.Vector3Field("Scale", t.localScale);
		
		EditorGUILayout.BeginHorizontal();
		
		if(GUILayout.Button("Copy"))
		{
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_pos_x", t.localPosition.x);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_pos_y", t.localPosition.y);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_pos_z", t.localPosition.z);
			
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_rot_x", t.localEulerAngles.x);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_rot_y", t.localEulerAngles.y);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_rot_z", t.localEulerAngles.z);

			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_scale_x", t.localScale.x);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_scale_y", t.localScale.y);
			PlayerPrefs.SetFloat(TRANSFORM_COPY_PREFIX + "_scale_z", t.localScale.z);
		}
		
		if(GUILayout.Button("Paste"))
		{
			Undo.RecordObject(t, "Transform Paste");
			
			// General object transform data found
			position = new Vector3(PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_pos_x"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_pos_y"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_pos_z"));
			rotation = new Vector3(PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_rot_x"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_rot_y"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_rot_z"));
			scale = new Vector3(PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_scale_x"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_scale_y"), PlayerPrefs.GetFloat(TRANSFORM_COPY_PREFIX + "_scale_z"));
		}
		if(GUILayout.Button("Set Identity"))
		{
			Undo.RecordObject(t, "Transform Set Identity");
			
			position = Vector3.zero;
			rotation = Vector3.zero;
			scale = Vector3.one;
		}
		EditorGUILayout.EndHorizontal();
		
		//EditorGUIUtility.LookLikeInspector();
		EditorGUIUtility.labelWidth = 80f;
		EditorGUIUtility.fieldWidth = 80f;

		if (GUI.changed)
		{
			Undo.RecordObject(t, "Transform Change");
			
			t.localPosition = position;
			t.localEulerAngles = rotation;
			t.localScale = scale;
		}
	}
}