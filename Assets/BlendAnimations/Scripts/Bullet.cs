﻿using UnityEngine;
using System.Collections;
using System.Linq.Expressions;

public class Bullet : MonoBehaviour
{
    public float Velocity;
    public RuntimeAnimatorController Controller;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(Velocity, 0, 0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(
            Random.Range(0,50),
            Random.Range(0, 50),
            Random.Range(0, 50));
    }
}

