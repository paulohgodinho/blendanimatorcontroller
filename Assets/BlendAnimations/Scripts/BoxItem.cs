﻿using UnityEngine;
using System.Collections;

public class BoxItem : PickableItem
{
    public bool IsPicked;
    private Transform _target;

    void Update()
    {
        if (IsPicked)
        {
            transform.position = Vector3.Lerp(transform.position, _target.transform.position, 1.0f);
        }
    }

    public override void ExecuteAction(CharData charData, Animator animator)
    {
        GetComponent<Collider>().enabled = false;
        _target = charData.GrabPosition;

        animator.SetTrigger("Pickup");
        Invoke("SetPick", 0.5f);
    }

    void SetPick()
    {
        IsPicked = true;
    }
}
