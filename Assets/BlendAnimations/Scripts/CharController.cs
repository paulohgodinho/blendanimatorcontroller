﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Experimental.Director;

[RequireComponent(typeof (CharacterController))]
public class CharController : MonoBehaviour
{

    public float MoveSpeed;
    public CharData Data;
    public PickableItem CurrentItem;
    
    private CharacterController _controller;
    private Animator _animator;
    private string _walkParameter = "Walking";
    
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        Data = GetComponent<CharData>();
    }

    void Update()
    {
        MoveLogic();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject item = GetItemInFront();
            if (item != null)
            {
                CurrentItem = item.GetComponent<PickableItem>();
                SwitchAnimators(CurrentItem.Controller);
                CurrentItem.ExecuteAction(Data, _animator);
            }
        }
    }

    private GameObject GetItemInFront()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 0.5f))
        {
            return hit.collider.gameObject;
        }

        return null;
    }

    private void SwitchAnimators(RuntimeAnimatorController animatorController)
    {
        var targetplayable = AnimatorControllerPlayable.Create(animatorController);
        _animator.Play(targetplayable);
    }

    private void MoveLogic()
    {
        _animator.SetBool(_walkParameter, false);

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            _controller.Move(Vector3.left * MoveSpeed * Time.deltaTime);
            _animator.SetBool(_walkParameter, true);
            transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _controller.Move(Vector3.right * MoveSpeed * Time.deltaTime);
            _animator.SetBool(_walkParameter, true);
            transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
        }
    }
}
