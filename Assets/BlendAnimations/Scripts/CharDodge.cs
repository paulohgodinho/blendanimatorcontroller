﻿using UnityEngine;
using System.Collections;
using UnityEngine.Experimental.Director;

public class CharDodge : MonoBehaviour
{

    private Animator _animator;
    public RuntimeAnimatorController DefaultController;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        DefaultController = _animator.runtimeAnimatorController;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Bullet>() != null)
        {
            SwitchAnimators(col.GetComponent<Bullet>().Controller);
        }    
    }

    private void SwitchAnimators(RuntimeAnimatorController animatorController)
    {
        var targetplayable = AnimatorControllerPlayable.Create(animatorController);
        _animator.Play(targetplayable);
    }

    public IEnumerator AnimationEnded()
    {
        yield return new WaitForEndOfFrame(); // Avoid flicker
        var targetplayable = AnimatorControllerPlayable.Create(DefaultController);
        _animator.Play(targetplayable);
    }
}
